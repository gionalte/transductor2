package com.gionalte.transductor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Vibrator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.gionalte.transductor.uihelper.UIHelper;

public class display3 extends AppCompatActivity implements UIHelper {

    private TextView noteTextView;
    private Thread audioThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        noteTextView = (TextView) findViewById(R.id.noteOutputTextView);
    }

    @Override
    public void display(final double pitch) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                noteTextView.setTextColor(Color.BLACK);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long patternDO[] = {0, 65, 65};
                long patternDOs[] = {0, 69, 69};
                long patternRE[] = {0, 73, 73};
                long patternREs[] = {0, 77, 77};
                long patternMI[] = {0, 82, 82};
                long patternFA[] = {0, 87, 87};
                long patternFAs[] = {0, 92, 92};
                long patternSOL[] = {0, 98, 98};
                long patternSOLs[] = {0, 103, 103};
                long patternLA[] = {0, 110, 110};
                long patternLAs[] = {0, 116, 116};
                long patternSI[] = {0, 123, 123};
                int dur = -1;

                if (pitch > 109.5 && pitch < 110.5) {//LA 110
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    v.vibrate(patternLA, dur);
                }
                if (pitch > 116 && pitch < 117) {//la# 116.5
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A#");
                    v.vibrate(patternLAs, dur);
                }
                if (pitch > 123 && pitch < 124) {//si 123.5
                    noteTextView.setBackgroundColor(Color.MAGENTA);
                    noteTextView.setText("B");
                    v.vibrate(patternSI, dur);
                }
                if (pitch > 130.3 && pitch < 131.3) {//do 130.8
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C");
                    v.vibrate(patternDO, dur);
                }
                if (pitch > 138.1 && pitch < 139.1) {//do# 138.6
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C#");
                    v.vibrate(patternDOs, dur);
                }
                if (pitch > 146.3 && pitch < 147.3) {//re 146.8
                    noteTextView.setBackgroundColor(Color.rgb(255, 128, 0));
                    noteTextView.setText("D");
                    v.vibrate(patternRE, dur);
                }
                if (pitch > 155.1 && pitch < 156.1) {//re# 155.6
                    noteTextView.setBackgroundColor(Color.rgb(255, 128, 0));
                    noteTextView.setText("D#");
                    v.vibrate(patternREs, dur);
                }
                if (pitch > 164.3 && pitch < 165.3) {//mi 164.8
                    noteTextView.setBackgroundColor(Color.YELLOW);
                    noteTextView.setText("E");
                    v.vibrate(patternMI, dur);
                }
                if (pitch > 174.1 && pitch < 175.1) {//fa 174.6
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F");
                    v.vibrate(patternFA, dur);
                }
                if (pitch > 184 && pitch < 186) {//fa# 185
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F#");
                    v.vibrate(patternFAs, dur);
                }
                if (pitch > 195 && pitch < 197) {//SOL 196
                    noteTextView.setBackgroundColor(Color.rgb(0, 128, 128));
                    noteTextView.setText("G");
                    v.vibrate(patternSOL, dur);
                }
                if (pitch > 207.2 && pitch < 208.2) {//SOL# 207.7
                    noteTextView.setBackgroundColor(Color.rgb(0, 128, 128));
                    noteTextView.setText("G#");
                    v.vibrate(patternSOLs, dur);
                }
                if (pitch > 219 && pitch < 221) {//LA 220
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    v.vibrate(patternLA, dur);
                }
                if (pitch > 232.6 && pitch < 233.6) {//LA# 233.1
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A#");
                    v.vibrate(patternLAs, dur);
                }
                if (pitch > 246.4 && pitch < 245.4) {//SI 246.9
                    noteTextView.setBackgroundColor(Color.MAGENTA);
                    noteTextView.setText("B");
                    v.vibrate(patternSI, dur);
                }
                if (pitch > 261.1 && pitch < 262.1) {//DO 261.6
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C");
                    v.vibrate(patternDO, dur);
                }
                if (pitch > 276.7 && pitch < 277.7) {//DO# 277.2
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C#");
                    v.vibrate(patternDOs, dur);
                }
                if (pitch > 293.2 && pitch < 294.2) {//RE 293.7
                    noteTextView.setBackgroundColor(Color.rgb(255, 128, 0));
                    noteTextView.setText("D");
                    v.vibrate(patternRE, dur);
                }
                if (pitch > 310.6 && pitch < 311.6) {//RE# 311.1
                    noteTextView.setBackgroundColor(Color.rgb(255, 128, 0));
                    noteTextView.setText("D#");
                    v.vibrate(patternREs, dur);
                }
                if (pitch > 329.1 && pitch < 330.1) {//MI 329.6
                    noteTextView.setBackgroundColor(Color.YELLOW);
                    noteTextView.setText("E");
                    v.vibrate(patternMI, dur);
                }
                if (pitch > 348.7 && pitch < 349.7) {//FA 349.2
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F");
                    v.vibrate(patternFA, dur);
                }
                if (pitch > 369 && pitch < 371) {//FA# 370
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F#");
                    v.vibrate(patternFAs, dur);
                }
                if (pitch > 391 && pitch < 393) {//SOL 392
                    noteTextView.setBackgroundColor(Color.rgb(0, 128, 128));
                    noteTextView.setText("G");
                    v.vibrate(patternSOL, dur);
                }
                if (pitch > 414.8 && pitch < 415.8) {//SOL# 415.3
                    noteTextView.setBackgroundColor(Color.rgb(0, 128, 128));
                    noteTextView.setText("G#");
                    v.vibrate(patternSOLs, dur);
                }
                if (pitch > 439 && pitch < 444) {//LA 440
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    v.vibrate(patternLA, dur);
                }
                if (pitch > 465.7 && pitch < 466.7) {//LA# 466.2
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A#");
                    v.vibrate(patternLAs, dur);
                }
                if (pitch > 493.4 && pitch < 494.4) {//SI 493.9
                    noteTextView.setBackgroundColor(Color.MAGENTA);
                    noteTextView.setText("B");
                    v.vibrate(patternSI, dur);
                }
                if (pitch > 522.8 && pitch < 523.8) {//DO 523.3
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C");
                    v.vibrate(patternDO, dur);
                }
                if (pitch > 553.9 && pitch < 554.9) {//DO# 554.4
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C#");
                    v.vibrate(patternDOs, dur);
                }
                if (pitch > 586.8 && pitch < 487.8) {//RE 587.3
                    noteTextView.setBackgroundColor(Color.rgb(255, 128, 0));
                    noteTextView.setText("D");
                    v.vibrate(patternRE, dur);
                }
                if (pitch > 621.8 && pitch < 622.8) {//RE# 622.3
                    noteTextView.setBackgroundColor(Color.rgb(255, 128, 0));
                    noteTextView.setText("D#");
                    v.vibrate(patternREs, dur);
                }
                if (pitch > 658.8 && pitch < 659.8) {//MI 659.3
                    noteTextView.setBackgroundColor(Color.YELLOW);
                    noteTextView.setText("E");
                    v.vibrate(patternMI, dur);
                }
                if (pitch > 698 && pitch < 699) {//FA 698.5
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F");
                    v.vibrate(patternFA, dur);
                }
                if (pitch > 739 && pitch < 741) {//FA# 740
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F#");
                    v.vibrate(patternFAs, dur);
                }
                if (pitch > 783 && pitch < 785) {//SOL 784
                    noteTextView.setBackgroundColor(Color.rgb(0, 128, 128));
                    noteTextView.setText("G");
                    v.vibrate(patternSOL, dur);
                }
                if (pitch > 830.1 && pitch < 831.1) {//SOL# 830.6
                    noteTextView.setBackgroundColor(Color.rgb(0, 128, 128));
                    noteTextView.setText("G#");
                    v.vibrate(patternSOLs, dur);
                }
                if (pitch > 879 && pitch < 881) {//LA 880
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    v.vibrate(patternLA, dur);
                }


              /*   else {
                    noteTextView.setText("CANTA");}*/

            }
        });}

    @Override
    protected void onPause() {
        endHook();
        super.onPause();
    }

    @Override
    protected void onResume() {
        startHook();
        super.onResume();
    }

    private void endHook() {
        com.gionalte.transductor.recording.AudioRecorder.deinit();
        try {
            audioThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private void startHook() {
        com.gionalte.transductor.recording.AudioRecorder.init(this);
        launchSynestapp();
    }

    private void launchSynestapp() {
        audioThread = new Thread(new Runnable() {
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_DEFAULT);
                com.gionalte.transductor.recording.AudioRecorder.run();
            }
        });

        audioThread.start();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.fullscreen){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
        switch (item.getItemId()) {
            case R.id.n1:
                startActivity(new Intent(this, display1.class));
                return true;
            case R.id.n2:
                startActivity(new Intent(this, display2.class));
                return true;
            case R.id.n3:
                startActivity(new Intent(this, display3.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }}
