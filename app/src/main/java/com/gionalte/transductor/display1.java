package com.gionalte.transductor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Process;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.gionalte.transductor.recording.AudioRecorder;
import com.gionalte.transductor.uihelper.UIHelper;


public class display1 extends AppCompatActivity implements UIHelper {

    private TextView noteTextView;
    private Thread audioThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
        @Override
        public void display(final double pitch) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    double freq = Math.round(pitch * 100) / 100.0;
                    noteTextView = (TextView) findViewById(R.id.noteOutputTextView);

                    int frec = (int) freq;
                    int tone1;
                    int tone2;
                    double color1;
                    double color2;
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    long pattern[] = {0, frec, frec};
                    int dur = -1;

                    //O1
                    if (freq >= 65.41 && freq < 82.41) {//do-mi rojo-amarillo
                        color2 = (frec - 65.41) * 15;
                        tone2 = (int) color2;
                        noteTextView.setBackgroundColor(Color.rgb(255, tone2, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 82.41 && freq < 87.31) {//mi-fa amarillo-verde
                        color1 = -(frec - 87.31) * 52.1;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 255, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 87.31 && freq < 110) {//fa-la verde-azul
                        color1 = -(frec - 110) * 11.20;
                        tone1 = (int) color1;
                        color2 = (frec - 87.31) * 11.20;
                        tone2 = (int) color2;
                        noteTextView.setBackgroundColor(Color.rgb(0, tone1, tone2));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 110 && freq < 123.47) {//la-si azul-morado
                        color1 = (frec - 110) * 18.93;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 0, 255));
                        //v.vibrate(pattern, dur);
                    }
                    if (freq >= 123.47 && freq < 130.81) {//si-do morado-rojo
                        color1 = -(frec - 130.81) * 34.74;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, 0, tone1));
                        v.vibrate(pattern, dur);
                    }

                    //O2
                    if (freq >= 130.81 && freq < 164.81) {//do-mi rojo-amarillo
                        color1 = (frec - 130.81) * 7.50;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, tone1, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 164.81 && freq < 174.61) {// mi-fa amarillo-verde
                        color1 = -(frec - 174.61) * 26.1;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 255, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 174.61 && freq < 220) {//fa-la verde-azul
                        color1 = -(frec - 220) * 5.60;
                        tone1 = (int) color1;
                        color2 = (frec - 174.61) * 5.60;
                        tone2 = (int) color2;
                        noteTextView.setBackgroundColor(Color.rgb(0, tone1, tone2));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 220 && freq < 246.94) {//la-si azul-morado
                        color1 = (frec - 220) * 9.46;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 0, 255));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 246.94 && freq < 261.63) {//si-do morado-rojo
                        color1 = -(frec - 261.63) * 17.35;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, 0, tone1));
                        v.vibrate(pattern, dur);
                    }

                    //O3
                    if (freq >= 261.63 && freq < 329.63) {//do-mi rojo-amarillo
                        color1 = (frec - 261.63) * 3.75;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, tone1, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 329.63 && freq < 349.23) {//mi-fa amarillo-verde
                        color1 = -(frec - 349.23) * 13.1;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 255, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 349.23 && freq < 440) {//fa-la verde-azul
                        color1 = -(frec - 440) * 2.80;
                        tone1 = (int) color1;
                        color2 = (frec - 349.23) * 2.80;
                        tone2 = (int) color2;
                        noteTextView.setBackgroundColor(Color.rgb(0, tone1, tone2));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 440 && freq < 493.88) {//la-si azul-morado
                        color1 = (frec - 440) * 4.73;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 0, 255));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 493.88 && freq < 523.25) {//si-do morado-rojo
                        color1 = -(frec - 523.25) * 8.68;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, 0, tone1));
                        v.vibrate(pattern, dur);
                    }

                    //O4
                    if (freq >= 523.25 && freq < 659.26) {//do-mi rojo-amarillo
                        color1 = (frec - 523.25) * 1.87;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, tone1, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 659.26 && freq < 698.46) {//mi-fa amarillo-verde
                        color1 = -(frec - 698.46) * 6.60;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 255, 0));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 698.46 && freq < 880) {//fa-la verde-azul
                        color1 = -(frec - 880) * 1.40;
                        tone1 = (int) color1;
                        color2 = (frec - 698.46) * 1.40;
                        tone2 = (int) color2;
                        noteTextView.setBackgroundColor(Color.rgb(0, tone1, tone2));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 880 && freq < 987.77) {//la-si azul-morado
                        color1 = (frec - 880) * 2.37;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(tone1, 0, 255));
                        v.vibrate(pattern, dur);
                    }
                    if (freq >= 987.77 && freq < 1046.50) {//si-do morado-rojo
                        color1 = -(frec - 1046.50) * 4.34;
                        tone1 = (int) color1;
                        noteTextView.setBackgroundColor(Color.rgb(255, 0, tone1));
                        v.vibrate(pattern, dur);
                    }



                  /*   else {
                        noteTextView.setText("CANTA");}*/

                }
            });}

        @Override
        protected void onPause() {
            endHook();
            super.onPause();
        }

        @Override
        protected void onResume() {
            startHook();
            super.onResume();
        }

        private void endHook() {
            AudioRecorder.deinit();
            try {
                audioThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.exit(-1);
            }
        }

        private void startHook() {
            AudioRecorder.init(this);
            launchSynestapp();
        }

        private void launchSynestapp() {
            audioThread = new Thread(new Runnable() {
                public void run() {
                    android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);
                    AudioRecorder.run();
                }
            });

            audioThread.start();
        }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.fullscreen){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
        switch (item.getItemId()) {
            case R.id.n1:
                startActivity(new Intent(this, display1.class));
                return true;
            case R.id.n2:
                startActivity(new Intent(this, display2.class));
                return true;
            case R.id.n3:
                startActivity(new Intent(this, display3.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }}

