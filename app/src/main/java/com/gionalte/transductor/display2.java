package com.gionalte.transductor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.gionalte.transductor.recording.AudioRecorder;
import com.gionalte.transductor.uihelper.UIHelper;


public class display2 extends AppCompatActivity implements UIHelper {


    private TextView noteTextView;
    private Thread audioThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        noteTextView = (TextView) findViewById(R.id.noteOutputTextView);
    }

    @Override
    public void display(final double pitch) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                noteTextView.setTextColor(Color.BLACK);
                Vibrator v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                int vbr;
                int dur=-1;

                if (pitch>108&&pitch<112){//LA 110
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    vbr=110;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern,dur);
                }
                if (pitch>114.5&&pitch<118.5){//la# 116.5
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A#");
                    vbr=116;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>121.5&&pitch<125.5){//si 123.5
                    noteTextView.setBackgroundColor(Color.MAGENTA);
                    noteTextView.setText("B");
                    vbr=123;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>129.8&&pitch<131.8){//do 130.8
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C");
                    vbr=130;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>136.6&&pitch<138.6){//do# 138.6
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C#");
                    vbr=138;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>140&&pitch<150){//re 146.8
                    noteTextView.setBackgroundColor(Color.rgb(255,128,0));
                    noteTextView.setText("D");
                    vbr=146;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>140&&pitch<150){//re# 155.6
                    noteTextView.setBackgroundColor(Color.rgb(255,128,0));
                    noteTextView.setText("D#");
                    vbr=155;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern,dur);
                }
                if (pitch>160&&pitch<170){//mi 164.8
                    noteTextView.setBackgroundColor(Color.YELLOW);
                    noteTextView.setText("E");
                    vbr=164;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>170&&pitch<180){//fa 174.6
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F");
                    vbr=174;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>180&&pitch<190){//fa# 185
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F#");
                    vbr=185;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>190&&pitch<200){//SOL 196
                    noteTextView.setBackgroundColor(Color.rgb(0,128,128));
                    noteTextView.setText("G");
                    vbr=196;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>200&&pitch<210){//SOL# 207.7
                    noteTextView.setBackgroundColor(Color.rgb(0,128,128));
                    noteTextView.setText("G#");
                    vbr=207;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>210&&pitch<230){//LA 220
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    vbr=220;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>230&&pitch<235){//LA# 233.1
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A#");
                    vbr=233;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>235&&pitch<250){//SI 246.9
                    noteTextView.setBackgroundColor(Color.MAGENTA);
                    noteTextView.setText("B");
                    vbr=246;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>255&&pitch<270){//DO 261.6
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C");
                    vbr=261;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>270&&pitch<285){//DO# 277.2
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C#");
                    vbr=277;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>285&&pitch<305){//RE 293.7
                    noteTextView.setBackgroundColor(Color.rgb(255,128,0));
                    noteTextView.setText("D");
                    vbr=293;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>305&&pitch<315){//RE# 311.1
                    noteTextView.setBackgroundColor(Color.rgb(255,128,0));
                    noteTextView.setText("D#");
                    vbr=311;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>315&&pitch<330){//MI 329.6
                    noteTextView.setBackgroundColor(Color.YELLOW);
                    noteTextView.setText("E");
                    vbr=329;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>335&&pitch<350){//FA 349.2
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F");
                    vbr=349;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>350&&pitch<385){//FA# 370
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F#");
                    vbr=370;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>385&&pitch<400){//SOL 392
                    noteTextView.setBackgroundColor(Color.rgb(0,128,128));
                    noteTextView.setText("G");
                    vbr=392;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>400&&pitch<435){//SOL# 415.3
                    noteTextView.setBackgroundColor(Color.rgb(0,128,128));
                    noteTextView.setText("G#");
                    vbr=415;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>435&&pitch<445){//LA 440
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    vbr=440;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>445&&pitch<490){//LA# 466.2
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A#");
                    vbr=466;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>490&&pitch<510){//SI 493.9
                    noteTextView.setBackgroundColor(Color.MAGENTA);
                    noteTextView.setText("B");
                    vbr=493;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>515&&pitch<530){//DO 523.3
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C");
                    vbr=523;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>530&&pitch<570){//DO# 554.4
                    noteTextView.setBackgroundColor(Color.RED);
                    noteTextView.setText("C#");
                    vbr=554;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>570&&pitch<600){//RE 587.3
                    noteTextView.setBackgroundColor(Color.rgb(255,128,0));
                    noteTextView.setText("D");
                    vbr=587;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>600&&pitch<640){//RE# 622.3
                    noteTextView.setBackgroundColor(Color.rgb(255,128,0));
                    noteTextView.setText("D#");
                    vbr=622;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>640&&pitch<670){//MI 659.3
                    noteTextView.setBackgroundColor(Color.YELLOW);
                    noteTextView.setText("E");
                    vbr=659;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>670&&pitch<710){//FA 698.5
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F");
                    vbr=698;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>710&&pitch<770){//FA# 740
                    noteTextView.setBackgroundColor(Color.GREEN);
                    noteTextView.setText("F#");
                    vbr=740;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>770&&pitch<800){//SOL 784
                    noteTextView.setBackgroundColor(Color.rgb(0,128,128));
                    noteTextView.setText("G");
                    vbr=784;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>800&&pitch<850){//SOL# 830.6
                    noteTextView.setBackgroundColor(Color.rgb(0,128,128));
                    noteTextView.setText("G#");
                    vbr=830;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern, dur);
                }
                if (pitch>850&&pitch<910){//LA 880
                    noteTextView.setBackgroundColor(Color.BLUE);
                    noteTextView.setText("A");
                    vbr=880;
                    long pattern[]={0,vbr,vbr};
                    v.vibrate(pattern,dur);
                }


              /*   else {
                    noteTextView.setText("CANTA");}*/

            } });}

    @Override
    protected void onPause() {
        endHook();
        super.onPause();
    }

    @Override
    protected void onResume() {
        startHook();
        super.onResume();
    }

    private void endHook() {
        AudioRecorder.deinit();
        try {
            audioThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private void startHook() {
        AudioRecorder.init(this);
        launchSynestapp();
    }

    private void launchSynestapp() {
        audioThread = new Thread(new Runnable() {
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_DEFAULT);
                AudioRecorder.run();
            }
        });

        audioThread.start();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.fullscreen){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
        switch (item.getItemId()) {
            case R.id.n1:
                startActivity(new Intent(this, display1.class));
                return true;
            case R.id.n2:
                startActivity(new Intent(this, display2.class));
                return true;
            case R.id.n3:
                startActivity(new Intent(this, display3.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }}



